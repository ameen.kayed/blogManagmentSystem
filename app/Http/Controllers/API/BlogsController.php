<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\blogRequest;
use App\Http\Requests\blogRequestEdit;
use App\Enums\BlogType;
use App\Models\Blogs;
use Illuminate\Support\Facades\Auth;
use App\Helpers\ResponseHelper;
use App\Http\Requests\blogRequestDelete;
use App\Http\Requests\commentRequest;
use App\Models\Comment;
use Illuminate\Support\Facades\Storage;

class BlogsController extends Controller
{

    /*
        function to get blog object using id 
    */
    public function show($id)
    {
        $blog = Blogs::where('id',$id)->first();
        
        if(!$blog)
        {
            return ResponseHelper::dataNotFound();
        }
       
        return ResponseHelper::select($blog);
    }

    /*
        function to get all blogs object
    */
    public function showAll($id)
    {
        $blog = Blogs::all();       
        return ResponseHelper::select($blog);
    }

    /*
        function to store blog object  
    */
    public function store(blogRequest $request)
    {
        $data = $request->all();
        
        //check if blog is Text OR Vedio to set correct type
        if($data['type'] == BlogType::Text){
            $blog = Blogs::create([
                'title'=>$data['title'],
                'details'=> $data['details'],
                'created_by'=> Auth::user()->id,
                'type'=> BlogType::Text,
            ]);

            //Send email to all signed Users for notify
            $this->sendMail();
            return ResponseHelper::operationSuccess();
        }
        else if($data['type'] == BlogType::Vedio){
            $results = $request->file('details')->store('blogsVedios');
            $blog = Blogs::create([
                'title'=>$data['title'],
                'details'=> $results,
                'created_by'=> Auth::user()->id,
                'type'=> BlogType::Vedio,
            ]);
            //Send email to all signed Users for notify
            $this->sendMail();
            return ResponseHelper::operationSuccess();
        }
        else{
            return ResponseHelper::invalidData();
        }
    }

    /*
        function to update blog object
    */
    public function update(blogRequestEdit $request)
    {
        $data = $request->all();
        $blog = Blogs::where('id',$data['blog_id'])->first();
        
        //check if blog not exists
        if(!$blog)
        {
            return ResponseHelper::dataNotFound();
        }

        //check if blog is Text OR Vedio to set correct type
        if($blog->type == BlogType::Text){
            
            //just update params get in request not all of object
            if(isset($request->title)){
                $blog->title = $data['title'];
            }

            if(isset($request->details)){
                $blog->details = $data['details'];
            }
            $blog->save();
            return ResponseHelper::operationSuccess();
        }
        else if($blog->type == BlogType::Vedio){

            //just update params get in request not all of object
            if(isset($request->title)){
                $blog->title = $data['title'];
            }

            if(isset($request->details)){

                //if blog has vedio then delete the video of the blog 
                if(Storage::exists($blog->details)){
                    Storage::delete($blog->details);
                }

                //add the new video of the blog 
                $results = $request->file('details')->store('blogsVedios');
                $blog->details = $results;
            }
            //save blog after update
            $blog->save();
            return ResponseHelper::operationSuccess();
        }
        else{
            return ResponseHelper::invalidData();
        }
    }

    /*
        function to delete blog object
    */
    public function delete(blogRequestDelete $request)
    {
        $data = $request->all();
        $blog = Blogs::where('id',$data['blog_id'])->first();
        
        //check if blog not exists
        if(!$blog)
        {
            return ResponseHelper::dataNotFound();
        }

        //delete the video of the blog 
        if(Storage::exists($blog->details)){
            Storage::delete($blog->details);
        }

        //delete the blog using softdelete 
        $blog->delete();
    }

    /*
        function to send bulk emails using queue job and redis 
    */
    public function sendMail()
    {
        $mail_data = [
            'subject' => "Notification"
        ];
        //call send email job
        $job = (new \App\Jobs\SendEmail($mail_data))
                ->delay(now()); 

        dispatch($job);
    }

    public function addComment(commentRequest $request)
    {
        $data = $request->all();
        $blog = Blogs::where('id',$data['blog_id'])->first();
        
        //check if blog not exists
        if(!$blog)
        {
            return ResponseHelper::dataNotFound();
        }

        Comment::create([
            'blog_id' => $blog->id,
            'comment' => $data['comment'],
            'created_by'=> Auth::user()->id,
        ]);

        return ResponseHelper::operationSuccess();
    }

}
